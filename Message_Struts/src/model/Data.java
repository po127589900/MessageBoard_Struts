package model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Data {
	public static final String SUCCESS="success";
	public static final String ERROR="error";

	public static String GetCurrentTime() {
		return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
	}
}
