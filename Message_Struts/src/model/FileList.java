package model;

import java.util.*;

public class FileList {
	private List<FileContent> files;

	public FileList() {
		files = new ArrayList<FileContent>();
	}

	public List<FileContent> getFiles() {
		return files;
	}

	public void setFiles(List<FileContent> files) {
		this.files = files;
	}

}
