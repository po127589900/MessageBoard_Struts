package action;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionContext;

import model.Data;
import model.Database;
import model.FileContent;
import model.FileList;

import org.apache.commons.io.*;

public class AddMessageAction {
	private static final long serialVersionUID = 1L;

	private String content;
	private List<File> file = new ArrayList<File>();
	private List<String> fileFileName = new ArrayList<String>();

	public String execute() throws Exception {
		String fileDir = "D:/Desktop/TestFileDir/";
		String savePath = "";
		FileList fileList = new FileList();
		Gson gson = new Gson();
		if (getFile() != null) {
			for (int i = 0; i < getFile().size(); i++) {
				String fileName = getFileFileName().get(i);
				savePath = fileDir + Data.GetCurrentTime() + fileName;
				File destFile = new File(savePath);
				FileUtils.copyFile(getFile().get(i), destFile);
				fileList.getFiles().add(new FileContent(fileName, savePath));
			}
		}

		String fileJson = gson.toJson(fileList).replaceAll("\"", "\\\\\"");
		Database db = new Database("messageBoard");
		db.Execute(String.format("insert into message(Username,Content,File) values(\"%s\",\"%s\",\"%s\")",
				ActionContext.getContext().getSession().get("username"), content, fileJson));
		return Data.SUCCESS;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<File> getFile() {
		return file;
	}

	public void setFile(List<File> file) {
		this.file = file;
	}

	public List<String> getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(List<String> fileFileName) {
		this.fileFileName = fileFileName;
	}

}
