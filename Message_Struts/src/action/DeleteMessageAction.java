package action;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionContext;

import model.Data;
import model.Database;
import model.FileContent;
import model.FileList;

public class DeleteMessageAction {
	private static final long serialVersionUID = 1L;
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String execute() {

		Database db = new Database("messageboard");
		ResultSet rs = db.Select(String.format("select * from message where id=%s", getId()));
		try {
			if (rs.next()) {
				Gson gson = new Gson();
				FileList fileList = gson.fromJson(rs.getString("File"), FileList.class);
				for (FileContent fileContent : fileList.getFiles()) {
					new File(fileContent.getFilePath()).delete();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.Execute(String.format("delete from message where Id=%s and Username=\"%s\"", getId(),
				ActionContext.getContext().getSession().get("username")));
		return Data.SUCCESS;
	}

}
