package action;

import java.io.IOException;
import java.sql.*;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import model.Account;
import model.Data;
import model.Database;

public class LoginAction extends ActionSupport implements ModelDriven<Account> {
	private static final long serialVersionUID = 1L;
	private Account account=new Account();
	
	public String execute() {
		if (IsAccount(account.getUsername(), account.getPassword())) {
			ActionContext.getContext().getSession().put("username", account.getUsername());
			return Data.SUCCESS;
		} else {
			((Map) ActionContext.getContext().get("request")).put("errorInput", "");
			return Data.ERROR;
		}
	}

	private boolean IsAccount(String username, String password) {
		Database db = new Database("messageboard");
		ResultSet rs = db.Select(
				String.format("SELECT * from account where Username=\"%s\" and Password=\"%s\"", username, password));
		try {
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Account getModel() {
		// TODO Auto-generated method stub
		return account;
	}

}
