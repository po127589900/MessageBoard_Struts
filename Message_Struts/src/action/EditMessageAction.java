package action;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionContext;

import model.*;

import java.sql.*;
import java.util.*;

public class EditMessageAction {
	private static final long serialVersionUID = 1L;

	private String id;
	private String content;
	private List<File> file = new ArrayList<File>();
	private List<String> fileFileName = new ArrayList<String>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<File> getFile() {
		return file;
	}

	public void setFile(List<File> file) {
		this.file = file;
	}

	public List<String> getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(List<String> fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String execute() {
		Database db = new Database("messageboard");
		String fileDir = "D:/Desktop/TestFileDir/";
		String username = (String) ActionContext.getContext().getSession().get("username");
		String savePath = "";
		Gson gson = new Gson();
		FileList fileList = new FileList();
		if (!username.isEmpty()) {
			if (getFile().isEmpty()) {
				db.Execute(String.format("update message set Content=\"%s\" where Id=%s and Username=\"%s\"",
						getContent(), getId(), username));
			} else {
				for (int i = 0; i < getFile().size(); i++) {
					String fileName = getFileFileName().get(i);
					savePath = fileDir + Data.GetCurrentTime() + fileName;
					File destFile = new File(savePath);
					try {
						FileUtils.copyFile(getFile().get(i), destFile);
						fileList.getFiles().add(new FileContent(fileName, savePath));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				ResultSet rs = db.Select(String.format("select * from message where id=%s", getId()));
				try {
					if (rs.next()) {
						FileList deleteFileList = gson.fromJson(rs.getString("File"), FileList.class);
						for (FileContent fileContent : deleteFileList.getFiles()) {
							new File(fileContent.getFilePath()).delete();
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				db.Execute(
						String.format("update message set Content=\"%s\",File=\"%s\" where Id=%s and Username=\"%s\"",
								getContent(), gson.toJson(fileList).replaceAll("\"", "\\\\\""), getId(), username));
			}
		}
		return Data.SUCCESS;
	}

}
