package action;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;

import model.Data;

public class LogoutAction {
	private static final long serialVersionUID = 1L;

	public String execute() {
		Map<String, Object> session = ActionContext.getContext().getSession();
		if (session.containsKey("username"))
			session.remove("username");
		return Data.SUCCESS;
	}

}
