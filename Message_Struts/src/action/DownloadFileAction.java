package action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import model.*;

public class DownloadFileAction {
	private static final long serialVersionUID = 1L;

	private String id;
	private String fileName;
	private FileInputStream fileInputStream;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public FileInputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(FileInputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String execute() {

		Database db = new Database("messageboard");
		ResultSet rs = db.Select(String.format("select * from message where id=%s", getId()));
		try {
			if (rs.next()) {
				Gson gson = new Gson();
				FileList fileList = gson.fromJson(rs.getString("File"), FileList.class);
				for (FileContent fileContent : fileList.getFiles()) {
					if (fileContent.getFileName().equals(getFileName())) {
						fileInputStream = new FileInputStream(new File(fileContent.getFilePath()));
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return Data.SUCCESS;
	}
}
